= CLI - Module Validation
:toc: 
:imagesdir: Resources/Docs/Images
:source-highlighter: prettify

== Project goals

Provide a https://github.com/JamesMc86/G-CLI[G CLI] custom step to run DQMH(R) module validator tool via command line interface.
This will allow you to integrate DQMH(R) module validation as part of your CI steps.

image::ToolCallInPoweShell.png[CLI]

== Supported environnement

* Compatible LabVIEW versions: 2016 or higher (32 or 64 bit).
* Compatible OS versions: Windows only

== Tool Documentation

=== Installing
The latest package is available via https://www.vipm.io/[VI Package Manager]

=== Command description

==== Call

.Command call
[source,c]
----
g-cli --lv-ver <LabVIEW version number> ValidateDqmhModules  -- // <1>
----
<1> paremeters Specific to the tool follow "--"


==== Parameters

[cols="1,1,1,2", options="header"] 
.Parameters
|===
|Name
|Category
|Required
|Description

|-pp
|Value
|Yes
|Path of the lvproj file you want to validate DQMH(R) modules
|===

==== Example

.Command with required parameters
[source,c]
----
g-cli --lv-ver 2020 ValidateDqmhModules -- -pp  "<lvproj path>"
----

==== Results

During the validation process the command output displays the current validation step.

If the validator detects one or more errors, the command finishes with a fail status and the list of the failures

NOTE: Any fix have to be done manualy or with the Validator tool UI (Select LabVIEW menu menu:Tools[DQMH Consortium > DQMH > Module > Validate DQMH Module…] 

== Dependencies

=== DQMH

this tool relies on DQMH 6.1 or above

=== G CLI

This tool rely on https://www.vipm.io/package/wiresmith_technology_lib_g_cli/[G CLI] toolkit developped by https://github.com/JamesMc86[James McNally]. You can find more information on calling parameters linked to G CLI https://github.com/JamesMc86/G-CLI/wiki/Getting-Started-With-G-CLI#full-command-line-interface[here]
